﻿namespace WebApplication12.Models
{
    public class ProductViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; } = "";
        public int Price { get; set; }
        public string CreatedDate { get; set; } = "";

        public ProductViewModel(int id, string name, int price, string createdDate)
        {
            ID = id;
            Name = name;
            Price = price;
            CreatedDate = createdDate;
        }
    }
}
