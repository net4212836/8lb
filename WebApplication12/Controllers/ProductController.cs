﻿using Microsoft.AspNetCore.Mvc;
using WebApplication12.Models;

namespace WebApplication12.Controllers
{
    public class ProductController : Controller
    {
        public IActionResult Index()
        {
            List<ProductViewModel> productList = new List<ProductViewModel>{
                new ProductViewModel(1, "Beans", 100, "12.08.2010"),
                new ProductViewModel(2, "Potato", 200, "21.10.2023"),
                new ProductViewModel(3, "Carrot", 500, "10.02.2020")
            };
            return View(productList);
        }
    }
}
